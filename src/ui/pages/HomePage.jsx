import React, { useState, useEffect } from "react";
import TodoInput from "../components/TodoInput";
import TodoList from "../components/TodoList";
import Loader from "../components/common/Loader";

export default function HomePage() {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setItems([
        { id: 1, text: "yep", isCompleted: false },
        { id: 2, text: "yes", isCompleted: true },
        { id: 3, text: "yet", isCompleted: false },
      ]);
      setLoading(false);
    }, 2500);
  }, []);

  return (
    <div className="homepage">
      <div className="wrapper">
        <TodoInput setItems={setItems} />
        {loading ? <Loader /> : <TodoList data={items} setItems={setItems} />}
      </div>
    </div>
  );
}
