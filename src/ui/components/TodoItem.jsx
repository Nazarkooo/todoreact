import React from "react";
import EditModal from "../views/editModal";
import { MdCheckBoxOutlineBlank, MdOutlineCheckBox } from "react-icons/md";
import { RiDeleteBin7Fill } from "react-icons/ri";
/* eslint react/prop-types: 0 */

export default function TodoItem({ itemData, setItems, dataItem }) {
  const isCompletedHandler = () => {
    setItems(
      dataItem.map((el) => {
        if (el.id == itemData.id) {
          el.isCompleted = !el.isCompleted;
          return el;
        } else {
          return el;
        }
      })
    );
  };

  const deleteHandler = () => {
    setItems((prev) => prev.filter((el) => el.id !== itemData.id));
  };
  return (
    <div className="todo-item__wrapper">
      {itemData.isCompleted ? (
        <p className="strike__throught">{itemData.text}</p>
      ) : (
        <p>{itemData.text}</p>
      )}
      <div className="todo-item-icons__wrapper">
        {itemData.isCompleted ? (
          <MdOutlineCheckBox
            onClick={isCompletedHandler}
            className="checkbox__completed"
          />
        ) : (
          <MdCheckBoxOutlineBlank
            onClick={isCompletedHandler}
            className="checkbox__uncompleted"
          />
        )}
        <EditModal
          setItems={setItems}
          editData={dataItem}
          itemData={itemData}
        />
        <RiDeleteBin7Fill onClick={deleteHandler} />
      </div>
    </div>
  );
}
