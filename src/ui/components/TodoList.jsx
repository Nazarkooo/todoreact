import React from "react";
import TodoItem from "../components/TodoItem";
/* eslint react/prop-types: 0 */

export default function TodoList({ data, setItems }) {
  function deleteAllHandler() {
    setItems([]);
  }

  function deleteDoneHandler() {
    setItems(
      data.filter((el) => {
        if (!el.isCompleted) return el;
      })
    );
  }

  return (
    <div className="todo-list__wrapper">
      <p>TodoList</p>
      {data.length == 0 ? (
        <p>NO TASKS</p>
      ) : (
        <div className="todo-list__items">
          {data.map((el, i) => (
            <TodoItem
              key={i}
              itemData={el}
              setItems={setItems}
              dataItem={data}
            />
          ))}
        </div>
      )}

      <div className="todo-list-btn__wrapper">
        <button onClick={deleteDoneHandler} className="delete-btn">
          Delete done tasks
        </button>
        <button onClick={deleteAllHandler} className="delete-btn">
          Delete all tasks
        </button>
      </div>
    </div>
  );
}
