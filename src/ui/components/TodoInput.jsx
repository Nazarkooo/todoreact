import React, { useRef } from "react";
/* eslint react/prop-types: 0 */

export default function TodoInput({ setItems }) {
  const inputEl = useRef(null);

  function handlerClick() {
    if (inputEl.current.value.trim() == "") return;

    const obj = {
      id: 7,
      text: inputEl.current.value,
      isCompleted: false,
    };

    setItems((current) => [...current, obj]);

    inputEl.current.value = "";
  }
  return (
    <div className="todo-input__wrapper">
      <p>TodoInput</p>
      <div className="add-new__task">
        <input ref={inputEl} type="text" placeholder="New Todo" />
        <button onClick={handlerClick}>Add new task</button>
      </div>
    </div>
  );
}
