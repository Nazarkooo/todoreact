import React, { useState, useRef } from "react";
import { BiPencil } from "react-icons/bi";

export default function editModal({ setItems, editData, itemData }) {
  const [isOpen, setIsOpen] = useState(false);
  const editInput = useRef(null);
  const editHandler = () => {
    if (editInput.current.value.trim() == "") return;
    console.log(1);
    setItems(
      editData.map((el) => {
        if (el.id == itemData.id) {
          console.log(2);
          el.text = editInput.current.value;
          return el;
        } else {
          return el;
        }
      })
    );

    setIsOpen(false);
  };
  return (
    <>
      <BiPencil onClick={() => setIsOpen(true)} />
      {isOpen && (
        <div className="modal">
          <div className="modal__body">
            <p>Write new text</p>
            <input ref={editInput} type="text" placeholder="Change Text" />
            <button onClick={editHandler}>Edit</button>
            <button className="delete-btn" onClick={() => setIsOpen(false)}>
              Exit
            </button>
          </div>
        </div>
      )}
    </>
  );
}
