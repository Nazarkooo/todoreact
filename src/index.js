import React from "react";
import ReactDOM from "react-dom/client";
import "./ui/style/index.scss";
import HomePage from "./ui/pages/HomePage";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <HomePage />
  </React.StrictMode>
);
